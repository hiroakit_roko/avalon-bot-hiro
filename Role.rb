class Role

  attr_accessor :display_name
  attr_accessor :describe
  attr_accessor :role_id
  attr_accessor :evil
  attr_accessor :pic_uri
  alias :evil? :evil

  KNIGHT = 0
  MARLIN = 1
  PERCIVAL = 2
  MINION = 3
  ASSASSIN = 4
  MORDRED = 5
  MORGANA = 6
  OBERON = 7


  def initialize(role_id)
    @role_id = role_id
    case role_id
    when KNIGHT
      @describe = "あなたはアーサーの忠実なる家来である。\n特殊な能力はないが、知力を尽くし任務を成功させよ。"
      @display_name = "アーサーの忠実なる家来"
      @evil = false
    when MARLIN
      @describe = "あなたは特別な力を持つマーリンである。\n邪悪を見通すことができるが、モードレッドを見破ることはできない。"
      @display_name = "マーリン"
      @evil = false
    when PERCIVAL
      @describe = "あなたは特別な力を持つパーシヴァルである。\nマーリンを知ることができるが、モルガナもマーリンに見えてしまう。"
      @display_name = "パーシヴァル"
      @evil = false
    when MINION
      @describe = "あなたはモードレッドの手下である。\n特殊な能力はないが、邪悪な味方とともに任務を失敗させよ。"
      @display_name = "モードレッドの手下"
      @evil = true
    when ASSASSIN
      @describe = "あなたは暗殺者である。\nゲーム敗北時、マーリンを暗殺できれば勝利することができる。"
      @display_name = "暗殺者"
      @evil = true
    when MORDRED
      @describe = "あなたはモードレッドである。\nあなたは敵のマーリンからも不可視となる。"
      @display_name = "モードレッド"
      @evil = true
    when MORGANA
      @describe = "あなたはモルガナである。\n敵のパーシヴァルからあなたはマーリンに見える。"
      @display_name = "モルガナ"
      @evil = true
    when OBERON
      @describe = "あなたはオベロンである。\nあなたは味方の邪悪もわからないし、味方からも邪悪だと認識されない。\nただし、マーリンのみはあなたが邪悪側であることを知っている。"
      @display_name = "オベロン"
      @evil = true
    end

    @pic_uri = "role_#{role_id}.JPG"

  end

  def ability(player_list)
    ab_str = ""
    case @role_id
    when MARLIN
      ab_str += "邪悪な影が見える････"
      player_list.each {|player|
        if player.role.evil? && player.role.role_id != MORDRED
          ab_str += "\n" + player.user_profile.display_name
        end
      }

    when PERCIVAL
      ab_str += "マーリンの姿が見える････"
      player_list.each {|player|
        if player.role.role_id == MARLIN || player.role.role_id == MORGANA
          ab_str += "\n" + player.user_profile.display_name
        end
      }

    when MINION, ASSASSIN, MORDRED, MORGANA
      ab_str += "邪悪な者たちを紹介しよう････"
      player_list.each {|player|
        if player.role.evil? && player.role.role_id != OBERON
          ab_str += "\n" + player.user_profile.display_name
        end
      }

    end

    return ab_str

  end

  def ability?()
    case @role_id
    when KNIGHT, OBERON
      return false
    else
      return true
    end

  end

  def to_s
    return @display_name
  end

end
