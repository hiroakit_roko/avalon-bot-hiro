class Mission

  attr_accessor :fail_num
  attr_accessor :total_num
  attr_accessor :result_str
  attr_accessor :disagreement
  attr_accessor :success

  def initialize
    @result_str = "未"
    @disagreement = 0
  end

  def disagree?
    disagree = false
    if @disagreement == 4
      disagree = true
      success(false)
      @disagreement = 0
    else
      @disagreement += 1
    end

    return disagree
  end

  def to_s
    str = ""
    if @fail_num == 2
      str = "[2枚で失敗]"
    end
    return "遠征[#{@total_num}]-#{@result_str + str}"
  end

  def disagreement_str
    return "現在#{disagreement + 1}回目の投票です。5回提案が却下されると任務失敗します。"
  end

  def success(is_success)
    if is_success
      @result_str = "成功"
    else
      @result_str = "失敗"
    end
    @success = is_success
  end

end
