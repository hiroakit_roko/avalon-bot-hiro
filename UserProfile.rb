class UserProfile

  attr_accessor :display_name
  attr_accessor :user_id
  attr_accessor :picture_url
  attr_accessor :status_message

  def to_s
    return "UserProfile[display_name=>#{display_name}, user_id=>#{user_id}, picture_url=>#{picture_url}, status_message=>#{status_message}]"
  end

end
