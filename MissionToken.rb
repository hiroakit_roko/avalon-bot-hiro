class MissionToken

  attr_accessor :success
  alias :success? :success
  attr_accessor :given_by_id
  attr_accessor :given_by_name

end
