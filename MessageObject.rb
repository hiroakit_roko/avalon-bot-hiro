class MessageObject

  attr_accessor :data
  attr_accessor :type

  TEXT = "text"
  IMAGE = "image"

  def initialize()
    @data = ""
    @type = TEXT
  end

  def to_message

    m = ""

    case type
    when TEXT

      m = {
        type: @type,
        text: @data
      }

    when IMAGE

      s_data = "s_" + @data

      url = $server_address + "/" + $IMG_FOLDER_NAME + "/" + @data
      s_url = $server_address + "/" + $IMG_FOLDER_NAME + "/" + s_data

      m = {
        type: @type,
        originalContentUrl: url,
        previewImageUrl: s_url
      }

    end

    return m

  end

end
