require './MissionGenerator'
class GameInfo

  attr_accessor :status
  attr_accessor :current
  attr_accessor :mission_list

  STATUS_JOIN = 0
  STATUS_VOTE = 1
  STATUS_MISSION = 2
  STATUS_ASSASSIN = 3
  STATUS_END = 4

  def initialize()
    @current = 0
  end

  def current_mission()
    return @mission_list[@current]
  end

  def next()
    @current += 1
  end

  def prepare_mission(player_num)
    generator = MissionGenerator.new(player_num)
    @mission_list = generator.mission_list
  end

  def mission_str()
    str = "ーミッション一覧ー"
    @mission_list.each_with_index {|mission, i|
      str += "\n"
      if @current == i
        str += "▶"
      else
        str += "　"
      end
      str += mission.to_s
    }
    return str
  end

  def lead_vote_str()
    str = @mission_list[@current].disagreement_str
    str += "\nリーダーは遠征チームを提案し、個別にBOTに承認または却下を入力してください。\n承認=【/vote approve】\n却下=【/vote reject】\nそれぞれ省略して、\n【/v a】や【/v r】でもOKです。"
    str += "\n" + mission_str()
    return str
  end

  def game_over?
    s = 0
    f = 0
    mission_list.each {|mission|
      if mission.result_str == "成功"
        s += 1
      elsif mission.result_str == "失敗"
        f += 1
      end
    }
    puts "ゲーム終了判定：成功#{s}回、失敗#{f}回"
    return s == 3 || f == 3
  end

  def blue_win?
    s = 0
    mission_list.each {|mission|
      if mission.result_str == "成功"
        s += 1
      end
    }
    return s == 3
  end

end
