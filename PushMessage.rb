
require 'line/bot'


class PushMessage

  def client
    @client ||= Line::Bot::Client.new { |config|
      config.channel_secret = ENV["LINE_CHANNEL_SECRET"]
      config.channel_token = ENV["LINE_CHANNEL_TOKEN"]
    }
  end

  def send(user_id, text)

    puts "push送信： user_id=#{user_id}, text=#{text}"
    client.push_message(user_id, text)
    puts "push送信終わり"

  end

  def sendInThread(user_id, text)
    Thread.new do
      send(user_id, text)
    end
  end
end
