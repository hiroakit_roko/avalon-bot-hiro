
require './AvalonResponseFactory'
require './PushMessage'
require './UserProfileAccesser'
require './RoleDeck'
require 'line/bot'

$IMG_FOLDER_NAME = "images"
$TEMP_FOLDER_NAME = "temp"
$server_address = "https://avalon-bot-hiro.herokuapp.com/"

def client
  @client ||= Line::Bot::Client.new { |config|
    config.channel_secret = ENV["LINE_CHANNEL_SECRET"]
    config.channel_token = ENV["LINE_CHANNEL_TOKEN"]
  }
end

req = %[{"events":[{"type":"message","replyToken":"c50b446112e24385bb21def70bad23f2","source":{"userId":"U564e09c8434f32ed73a1835820cc026c","type":"user"},"timestamp":1490509508484,"message":{"type":"text","id":"5839905201264","text":"/s gs"}}]}]

events = client.parse_events_from(req)

message = ["/make", "/make a s","/make aaa"]

 # userId = U564e09c8434f32ed73a1835820cc026c

message.each{ |str|
  events.each{|event|

    event.message['text'] = str
    factory = AvalonResponseFactory.new(event)

    puts "reply?=#{factory.reply?()}"
    puts factory.reply().data

  }
}

deck = RoleDeck.new(6)
puts deck.instruction
