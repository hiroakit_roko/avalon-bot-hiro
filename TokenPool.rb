require './ApprovalToken'
require './MissionToken'
class TokenPool
  attr_reader :approval_token_list
  attr_reader :mission_token_list

  def initialize()
    @approval_token_list = Array.new
    @mission_token_list = Array.new
  end

  def clear()
    @approval_token_list = Array.new
    @mission_token_list = Array.new
  end

  def enough_approval_token?(player_num)
    return @approval_token_list.size >= player_num
  end

  def accept_approval_token(approved, id, name)
    token = ApprovalToken.new
    token.approved = approved
    token.given_by_id = id
    token.given_by_name = name
    @approval_token_list.push(token)
    @approval_token_list.shuffle!
  end

  def enough_mission_token?(player_num)
    return @mission_token_list.size >= player_num
  end

  def accept_mission_token(success, id, name)
    token = MissionToken.new
    token.success = success
    token.given_by_id = id
    token.given_by_name = name
    @mission_token_list.push(token)
    @mission_token_list.shuffle!
  end

  def approved?()
    approved_sum = 0.0
    @approval_token_list.each {|token|
      if token.approved?
        approved_sum += 1.0
      end
    }
    puts "承認=#{approved_sum} 人数=#{@approval_token_list.size}"
    if approved_sum > (@approval_token_list.size / 2.0)
      return true
    else
      return false
    end
  end

  def result_approval()
    result = ""
    if approved?
      result = "可決"
    else
      result = "否決"
    end
    str = "ー承認結果：#{result}ー"
    @approval_token_list.each {|token|
      str += "\n#{token.given_by_name}:"
      if token.approved?
        str += "承認"
      else
        str += "却下"
      end
    }
    return str
  end

  def to_s(fail_num)
    return result_approval() + "\n" + result_mission(fail_num)
  end

  def result_mission(fail_num)
    result = ""
    if success?(fail_num)
      result = "成功"
    else
      result = "失敗"
    end
    str = "ー任務結果：#{result}ー\n"
    str += "【#{mission_token_list.map {|token|token.given_by_name}.shuffle.join(",")}】"
    @mission_token_list.each {|token|
      str += "\n"
      if token.success?
        str += "成功"
      else
        str += "失敗"
      end
    }
    return str
  end



  def success?(fail_num)
    fail_sum = 0
    @mission_token_list.each {|token|
      unless token.success?
        fail_sum += 1
      end
    }
    if fail_sum >= fail_num
      return false
    else
      return true
    end
  end

end
