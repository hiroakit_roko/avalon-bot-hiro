require 'line/bot'
require 'json'
require './UserProfile'

class UserProfileAccesser

  def client
    @client ||= Line::Bot::Client.new { |config|
      config.channel_secret = ENV["LINE_CHANNEL_SECRET"]
      config.channel_token = ENV["LINE_CHANNEL_TOKEN"]
    }
  end

  def user_profile(user_id)
    json = JSON.parse(client.get_profile(user_id).body)
    prof = UserProfile.new
    prof.display_name = json['displayName']
    prof.user_id = json['userId']
    prof.picture_url = json['pictureUrl']
    prof.status_message = json['statusMessage']
    return prof
  end
end
