class Town

  attr_accessor :town_id
  attr_accessor :group_id
  attr_accessor :chat_type
  attr_accessor :game_info
  attr_accessor :player_list
  attr_accessor :token_pool
  attr_accessor :log
  attr_accessor :deck_type

  def initialize(town_id, group_id, chat_type)
    @town_id = town_id
    @group_id = group_id
    @chat_type = chat_type
    @game_info = GameInfo.new
    @game_info.status = GameInfo::STATUS_JOIN
    @player_list = Array.new
    @log = Array.new
    @deck_type = 1
  end

  def leader_str
    str = "ーリーダー表示ー"
    @player_list.each {|player|
      str += "\n"
      if player.leader?
        str += "▶"
      else
        str += "　"
      end
      str += player.user_profile.display_name
      if player.co?
        str += "［パ］"
      end
    }
    return str
  end

  def next_leader()
    @player_list.each_with_index {|player, i|
      if player.leader?
        player.leader = false
        unless i == (@player_list.size - 1)
          @player_list[i+1].leader = true
        else
          @player_list[0].leader = true
        end
        break
      end
    }
  end

  def find_user_name(id)
    person = @player_list.find {|player|
      player.user_profile.user_id == id
    }
    return person.user_profile.display_name
  end

  def find_player(id)
    person = @player_list.find {|player|
      player.user_profile.user_id == id
    }
    return person
  end

  def has_assassin?()
    person = @player_list.find {|player|
      player.role.role_id == Role::ASSASSIN
    }
    return !person.nil?
  end

  def find_assassin_id()
    person = @player_list.find {|player|
      player.role.role_id == Role::ASSASSIN
    }
    return person.user_profile.user_id
  end

  def kill_candidates()
    list = @player_list.select {|player|
      (!player.role.evil?) || (player.role.role_id == Role::OBERON)
    }
    return list
  end

  def player_num
    return @player_list.size
  end

  def game_over?
    return @game_info.game_over?
  end

  def blue_win?
    return @game_info.blue_win?
  end

  def show_roles
    str = "ー配役ー"
    @player_list.each {|player|
      str += "\n" + player.user_profile.display_name + ": " + player.role.display_name
    }
    return str
  end

end
