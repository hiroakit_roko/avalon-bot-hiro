require './Mission'
class MissionGenerator

  MISSION_SETTING = [
    [[2, 1], [3, 1], [2, 1], [3, 1], [3, 1]], # 5人ゲーム
    [[2, 1], [3, 1], [4, 1], [3, 1], [4, 1]], # 6人ゲーム
    [[2, 1], [3, 1], [3, 1], [4, 2], [4, 1]], # 7人ゲーム
    [[3, 1], [4, 1], [4, 1], [5, 2], [5, 1]], # 8人ゲーム
    [[3, 1], [4, 1], [4, 1], [5, 2], [5, 1]], # 9人ゲーム
    [[3, 1], [4, 1], [4, 1], [5, 2], [5, 1]] # 10人ゲーム
  ]

  attr_accessor :mission_list

  def initialize(player_num)
    @mission_list = Array.new
    fixed_list = MISSION_SETTING[player_num - 5]
    fixed_list.each {|setting|
      mission = Mission.new
      mission.total_num = setting[0]
      mission.fail_num = setting[1]
      @mission_list.push(mission)
    }
  end

end
