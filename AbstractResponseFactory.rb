
class AbstractResponseFactory

  def initialize(event)
    msg = event.message['text']
    msg.strip!()
    @header = msg.chr()
    msg.slice!(0)
    res = msg.split(" ")
    @command = res.first()
    @params = res.drop(1)

    @from_id = ""
    @from_type = event['source']['type']
    case @from_type
    when 'user'
      @from_id = event['source']['userId']
    when 'group'
      @from_id = event['source']['groupId']
    when 'room'
      @from_id = event['source']['roomId']
    end

  end

  def reply?()
    return @header == "/"
  end



  def to_s()
    return "header: #{@header}, command: #{@command}, param: #{@params}, from_id#{@from_id}, from_type#{@from_type} "
  end

  # 抽象メソッド
  def reply()

    raise "Called abstract method: reply"

  end


end
