class ApprovalToken

  attr_accessor :approved
  alias :approved? :approved
  attr_accessor :given_by_id
  attr_accessor :given_by_name

end
