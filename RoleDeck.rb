require './Role'
class RoleDeck

  DECK_STANDARD = [[Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::ASSASSIN, Role::MORGANA], # 5人プレイ
  [Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::ASSASSIN, Role::MORDRED], # 6人プレイ
  [Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::OBERON, Role::ASSASSIN, Role::MORDRED], # 7人プレイ
  [Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::MINION, Role::ASSASSIN, Role::MORDRED], # 8人プレイ
  [Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::ASSASSIN, Role::MORDRED, Role::MORGANA], # 9人プレイ
  [Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::MINION, Role::ASSASSIN, Role::MORDRED, Role::OBERON] # 10人プレイ
]

DECK_SIMPLE = [[Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::MINION, Role::ASSASSIN], # 5人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::MINION, Role::ASSASSIN], # 6人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::MINION, Role::MINION, Role::ASSASSIN], # 7人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::MINION, Role::MINION, Role::ASSASSIN], # 8人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::MINION, Role::MINION, Role::ASSASSIN], # 9人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::MINION, Role::MINION, Role::MINION, Role::ASSASSIN] # 10人プレイ
]

DECK_ALTERNATIVE = [[Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::ASSASSIN, Role::MORDRED], # 5人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::ASSASSIN, Role::MORGANA], # 6人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::OBERON, Role::ASSASSIN, Role::MORGANA], # 7人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::OBERON, Role::ASSASSIN, Role::MORGANA], # 8人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::ASSASSIN, Role::MORDRED, Role::OBERON], # 9人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::MORGANA, Role::ASSASSIN, Role::MORDRED, Role::OBERON] # 10人プレイ
]

DECK_INSANE = [[Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::ASSASSIN, Role::OBERON], # 5人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::ASSASSIN, Role::OBERON], # 6人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::ASSASSIN, Role::MORDRED, Role::MORGANA], # 7人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::MORDRED, Role::ASSASSIN, Role::MORGANA], # 8人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::ASSASSIN, Role::MORGANA, Role::OBERON], # 9人プレイ
[Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::KNIGHT, Role::MARLIN, Role::PERCIVAL, Role::MINION, Role::MORGANA, Role::ASSASSIN, Role::MORDRED] # 10人プレイ
]

def initialize(player_num, deck_type)

  @role_list = Array.new

  case deck_type
  when 1
    DECK_STANDARD[player_num - 5].each {|role_id|
      @role_list.push(Role.new(role_id))
    }
  when 2
    DECK_SIMPLE[player_num - 5].each {|role_id|
      @role_list.push(Role.new(role_id))
    }
  when 3
    DECK_ALTERNATIVE[player_num - 5].each {|role_id|
      @role_list.push(Role.new(role_id))
    }
  when 4
    DECK_INSANE[player_num - 5].each {|role_id|
      @role_list.push(Role.new(role_id))
    }
  end

end

def to_s
  str = @role_list.join("\n")
  return str
end

def shuffle!
  @role_list.shuffle!
end

def instruction
  jus_num = 0
  evil_num = 0
  @role_list.each {|role|
    if role.evil?
      evil_num += 1
    else
      jus_num += 1
    end
  }
  role_str = "今回の配役は青チーム#{jus_num}人、赤チーム#{evil_num}人です。\n#{self.to_s}"

  return role_str
end

def role_list
  return @role_list
end

def self.deck_by_playernum(player_num)
  str = "＊#{player_num}人プレイ用のデッキ＊\n"
  str += "<deck 1 -STANDARD->\n"
  str += DECK_STANDARD[player_num - 5].map{|role_id| Role.new(role_id).display_name}.join("\n")
  str += "\n\n<deck 2 -SIMPLE->\n"
  str += DECK_SIMPLE[player_num - 5].map{|role_id| Role.new(role_id).display_name}.join("\n")
  str += "\n\n<deck 3 -ALTERNATIVE->\n"
  str += DECK_ALTERNATIVE[player_num - 5].map{|role_id| Role.new(role_id).display_name}.join("\n")
  str += "\n\n<deck 4 -INSANE->\n"
  str += DECK_INSANE[player_num - 5].map{|role_id| Role.new(role_id).display_name}.join("\n")
  return str
end

end
