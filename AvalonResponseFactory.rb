require './GameInfo'
require './Town'
require './Player'
require './AbstractResponseFactory'
require './MessageObject'
require './PushMessage'
require './UserProfileAccesser'
require './TokenPool'
require './Player'
require './Role'
require './Note'
require './RoleDeck'

class AvalonResponseFactory < AbstractResponseFactory

  @@town_hash = Hash.new
  @@routing_table = Hash.new

  $debug_mode = false

  def reply()

    command_down = @command.downcase()

    res = nil

    case command_down

    when "m", "make"

      res = make()

    when "j", "join"

      res = join()

    when "s", "start"

      res = start()

    when "i", "instruction"

      res = instruction()

    when "v", "vote"

      res = vote()

    when "q", "quest"

      res = quest()

    when "k", "kill"

      res = kill()

    when "h", "help"

      res = help()

    when "?"

      res = question()

    when "l", "log"

      res = log()

    when "n", "note"

      res = note()

    when "c", "co"

      res = co()

    when "d", "deck"

      res = deck()

    when "d?", "deck?"

      res = deck_question()

    else

      case @command

      when "DEBUG"

        res = debug()

      else

        res = MessageObject.new
        res.data = "コマンドが見つかりません。 コマンド名=#{@command}"

      end

    end

    return res

  end

  private
  def make()

    if @from_type == "user"

      res = MessageObject.new
      res.data = "村の作成はメインのグループチャット内から行ってください。 コマンド名=#{@command}"
      return res

    end

    unless @params.size == 1

      res = MessageObject.new
      res.data = "引数の数が不正です。村のIDを入力してください。 コマンド名=#{@command}"
      return res

    end

    if @@town_hash.include?(@params[0])

      res = MessageObject.new
      res.data = "すでに村のIDが存在しています。他のIDを指定してください。 コマンド名=#{@command}"
      return res

    end

    town = Town.new(@params[0], @from_id, @from_type)

    @@town_hash[town.town_id] = town
    @@routing_table[@from_id] = town.town_id

    res = MessageObject.new
    res.data = "新しい村（ID=【#{town.town_id}】）を作成しました。\n個別にBOTに話しかけて入室してください。\nコマンド=【/join #{town.town_id}】"

    return res

  end

  private
  def join()

    unless @from_type == "user"

      res = MessageObject.new
      res.data = "村への参加は個別チャットから行ってください。 コマンド名=#{@command}"
      return res

    end

    unless @params.size == 1

      res = MessageObject.new
      res.data = "引数の数が不正です。村のIDを入力してください。 コマンド名=#{@command}"
      return res
    end

    unless @@town_hash.include?(@params[0])

      res = MessageObject.new
      res.data = "指定された村のIDが存在しません。 コマンド名=#{@command}"
      return res

    end

    town = @@town_hash[@params[0]]

    unless town.game_info.status == GameInfo::STATUS_JOIN

      res = MessageObject.new
      res.data = "すでにゲームがはじまっています。 コマンド名=#{@command}"
      return res

    end

    unless $debug_mode

      if town.player_list.any? {|player| player.user_profile.user_id == @from_id}

        res = MessageObject.new
        res.data = "すでにその村には入室しています。 コマンド名=#{@command}"
        return res

      end

    end

    if town.player_list.size >= 10

      res = MessageObject.new
      res.data = "すでに10人が参加しているため、参加できません。 コマンド名=#{@command}"
      return res

    end

    user_info = Player.new
    accs = UserProfileAccesser.new
    user_info.user_profile = accs.user_profile(@from_id)
    town.player_list.push(user_info)
    @@routing_table[@from_id] = town.town_id

    push = PushMessage.new
    push_obj = MessageObject.new
    push_obj.data = "#{user_info.user_profile.display_name}さんが入室しました。\n現在#{town.player_list.size}人です。"
    if town.player_list.size >= 5
      push_obj.data = push_obj.data + "\n開始するには【/start】を入力してください。"
    end
    push.sendInThread(town.group_id, push_obj.to_message)

    res = MessageObject.new
    res.data = "入室に成功しました。"

    return res

  end

  private
  def start()

    if @from_type == "user"

      res = MessageObject.new
      res.data = "ゲームの開始はメインのグループチャット内から行ってください。 コマンド名=#{@command}"
      return res

    end

    if @@town_hash.include?(@from_id)

      res = MessageObject.new
      res.data = "村が存在しません。まずは村の作成を行ってください。\n村の作成コマンド=【/make 村ID】"
      return res

    end

    town = town_by_id()

    unless town.player_list.size.between?(5, 10)
      res = MessageObject.new
      res.data = "ゲームプレイ可能人数は5～10人です。\n人数が揃ってから開始してください。 コマンド名=#{@command}"
      return res
    end

    unless town.game_info.status == GameInfo::STATUS_JOIN

      res = MessageObject.new
      res.data = "すでにゲームがはじまっています。 村ID=#{town.town_id}"
      return res

    end

    town.game_info.status = GameInfo::STATUS_VOTE

    deck = RoleDeck.new(town.player_list.size, town.deck_type)

    push = PushMessage.new
    push_obj = MessageObject.new
    push_obj.data = deck.instruction
    push.send(town.group_id, push_obj.to_message)

    push = PushMessage.new
    push_obj = MessageObject.new
    push_obj.data = "5秒後にゲームを開始します..."
    push.send(town.group_id, push_obj.to_message)

    sleep 5

    role_list = deck.role_list
    role_list.shuffle!
    role_list.each_with_index {|role, i|
      town.player_list[i].role = role
      user_id = town.player_list[i].user_profile.user_id

      role_str = MessageObject.new
      role_str.data = role.describe
      push.sendInThread(user_id, role_str.to_message)

      role_pic = MessageObject.new
      role_pic.data = role.pic_uri
      role_pic.type = MessageObject::IMAGE
      push.sendInThread(user_id, role_pic.to_message)
    }
    # 能力発動
    town.player_list.each {|player|
      if player.role.ability?
        role_abl = MessageObject.new
        role_abl.data = player.role.ability(town.player_list)
        push.sendInThread(player.user_profile.user_id, role_abl.to_message)
      end
    }

    # 全体図を表示
    push_overall = MessageObject.new
    push_overall.data = "overall_view_#{town.player_list.size}.JPG"
    push_overall.type = MessageObject::IMAGE
    push.send(town.group_id, push_overall.to_message)

    town.player_list = town.player_list.shuffle
    town.player_list[0].leader = true

    # ミッション生成
    town.game_info.prepare_mission(town.player_list.size)

    town.token_pool = TokenPool.new

    res = MessageObject.new
    res.data = town.game_info.lead_vote_str + "\n" + town.leader_str

    return res

  end

  private
  def send_game_info(id, town)

    push = PushMessage.new
    leader_str = MessageObject.new
    leader_str.data = town.game_info.lead_vote_str + "\n" + town.leader_str
    push.send(id, leader_str.to_message)

  end

  private
  def vote()

    unless @from_type == "user"

      res = MessageObject.new
      res.data = "投票は個別チャットから行ってください。 コマンド名=#{@command}"
      return res

    end

    unless @@routing_table.include?(@from_id)
      res = MessageObject.new
      res.data = "入場している村がありません。 コマンド名=#{@command}"
      return res
    end

    unless @params.size == 1
      res = MessageObject.new
      res.data = "引数の数が不正です。\n承認=【approve】または【a】\n却下=【reject】または【r】\nを引数に入力してください。 コマンド名=#{@command}"
      return res
    end

    param = @params[0].downcase()

    unless ["a", "approve", "r", "reject"].include?(param)
      res = MessageObject.new
      res.data = "引数の値が不正です。\n承認=【approve】または【a】\n却下=【reject】または【r】\nを引数に入力してください。コマンド名=#{@command}"
      return res
    end

    town = town_by_id()

    unless $debug_mode

      if town.token_pool.approval_token_list.any? {|token| token.given_by_id == @from_id}

        res = MessageObject.new
        res.data = "すでに投票済です。 コマンド名=#{@command}"
        return res

      end

    end

    unless town.game_info.status == GameInfo::STATUS_VOTE
      res = MessageObject.new
      res.data = "現在は投票を受け付けていません。コマンド名=#{@command}"
      return res
    end

    case param
    when "a", "approve"
      town.token_pool.accept_approval_token(true, @from_id, town.find_user_name(@from_id))
    when "r", "reject"
      town.token_pool.accept_approval_token(false, @from_id, town.find_user_name(@from_id))
    else
      res = MessageObject.new
      res.data = "論理的にありえない！！エラーです。コマンド名=#{@command}"
      return res
    end

    puts town.token_pool.to_s(town.game_info.current_mission.fail_num)

    push = PushMessage.new
    push_obj = MessageObject.new

    if town.token_pool.enough_approval_token?(town.player_num)

      note = Note.new
      leader = town.player_list.find{|player| player.leader?}.user_profile.display_name
      note.text = "#{town.game_info.current + 1}回目のミッション、#{town.game_info.current_mission.disagreement + 1}回目の提案<L:#{leader}>\n#{town.token_pool.result_approval}\n"
      note.from_id = town.group_id
      note.from_type = "system"
      town.log.push(note)

      push_obj.data = town.token_pool.result_approval
      push.send(town.group_id, push_obj.to_message)
      if town.token_pool.approved?

        result_pic = MessageObject.new
        result_pic.data = "approve.JPG"
        result_pic.type = MessageObject::IMAGE
        push.send(town.group_id, result_pic.to_message)

        town.game_info.status = GameInfo::STATUS_MISSION
        push_obj.data = "ミッションに選ばれた人は、BOTに個別に\n成功【/quest success】\n失敗【/quest fail】\nを送信してください。\n【/q s】または【/q f】でも構いません。"
        push.send(town.group_id, push_obj.to_message)
      else

        result_pic = MessageObject.new
        result_pic.data = "reject.JPG"
        result_pic.type = MessageObject::IMAGE
        push.send(town.group_id, result_pic.to_message)

        # disagree?内でカウンタもアップ
        if town.game_info.current_mission.disagree?
          push_obj.data = "否決が5回に達したため、任務に失敗しました。"
          if town.game_over?
            game_over(town)
          else
            # 否決して次のミッションへ
            town.next_leader
            town.game_info.next()
            push_obj.data += "\n次のミッションに進みます。"
            push.send(town.group_id, push_obj.to_message)
            send_game_info(town.group_id, town)
          end
        else
          town.next_leader
          push_obj.data = "もう一度投票をやり直します。"
          push.send(town.group_id, push_obj.to_message)
          send_game_info(town.group_id, town)
        end
      end
      town.token_pool.clear
    else
      push_obj.data = "投票を受け付けました。あと#{town.player_num - town.token_pool.approval_token_list.size}人です。"
      push.send(town.group_id, push_obj.to_message)
    end

    res = MessageObject.new
    res.data = "投票を受け付けました。"

    return res

  end

  private
  def instruction()

    unless @params.size == 1

      res = MessageObject.new
      res.data = "引数の数が不正です。\n説明書番号（0..5）を指定してください。 コマンド名=#{@command}"
      return res
    end

    unless @params[0].to_i.between?(0, 5)

      res = MessageObject.new
      res.data = "引数は0..5で入力してください。 コマンド名=#{@command}"
      return res

    end

    file_name = "instruction_#{@params[0]}.JPG"

    res = MessageObject.new
    res.data = file_name
    res.type = MessageObject::IMAGE

    return res

  end

  private
  def quest()

    unless @from_type == "user"

      res = MessageObject.new
      res.data = "任務は個別チャットから行ってください。 コマンド名=#{@command}"
      return res

    end

    unless @@routing_table.include?(@from_id)
      res = MessageObject.new
      res.data = "入場している村がありません。 コマンド名=#{@command}"
      return res
    end

    unless @params.size == 1
      res = MessageObject.new
      res.data = "引数の数が不正です。\n成功=【success】または【s】\n失敗=【fail】または【f】\nを引数に入力してください。 コマンド名=#{@command}"
      return res
    end

    param = @params[0].downcase()

    unless ["s", "success", "f", "fail"].include?(param)
      res = MessageObject.new
      res.data = "引数の値が不正です。\n成功=【success】または【s】\n失敗=【fail】または【f】\nを引数に入力してください。コマンド名=#{@command}"
      return res
    end

    town = town_by_id()

    unless town.game_info.status == GameInfo::STATUS_MISSION
      res = MessageObject.new
      res.data = "現在は任務を受け付けていません。コマンド名=#{@command}"
      return res
    end

    unless $debug_mode

      if town.token_pool.mission_token_list.any? {|token| token.given_by_id == @from_id}

        res = MessageObject.new
        res.data = "すでに提出済です。 コマンド名=#{@command}"
        return res

      end

    end

    case param
    when "s", "success"
      town.token_pool.accept_mission_token(true, @from_id, town.find_user_name(@from_id))
    when "f", "fail"
      town.token_pool.accept_mission_token(false, @from_id, town.find_user_name(@from_id))
    else
      res = MessageObject.new
      res.data = "論理的にありえない！！エラーです。コマンド名=#{@command}"
      return res
    end

    puts town.token_pool.to_s(town.game_info.current_mission.fail_num)

    push = PushMessage.new
    push_obj = MessageObject.new

    if town.token_pool.enough_mission_token?(town.game_info.current_mission.total_num)

      note = Note.new
      note.from_id = town.group_id
      note.from_type = "system"
      note.text = "#{town.game_info.current + 1}回目のミッション\n#{town.token_pool.result_mission(town.game_info.current_mission.fail_num)}\n\n"
      town.log.push(note)

      push_obj.data = town.token_pool.result_mission(town.game_info.current_mission.fail_num)
      push.send(town.group_id, push_obj.to_message)
      if town.token_pool.success?(town.game_info.current_mission.fail_num)

        push_obj.data = "任務成功。"

        result_pic = MessageObject.new
        result_pic.data = "success.JPG"
        result_pic.type = MessageObject::IMAGE
        push.send(town.group_id, result_pic.to_message)

        town.game_info.current_mission.success(true)

        if town.game_over?
          game_over(town)
        else
          town.game_info.status = GameInfo::STATUS_VOTE
          town.game_info.next
          town.next_leader
          push_obj.data += "\n次のミッションに進みます。"
          push.send(town.group_id, push_obj.to_message)
          send_game_info(town.group_id, town)
        end

      else

        push_obj.data = "任務失敗。"

        result_pic = MessageObject.new
        result_pic.data = "fail.JPG"
        result_pic.type = MessageObject::IMAGE
        push.send(town.group_id, result_pic.to_message)

        town.game_info.current_mission.success(false)

        if town.game_over?
          game_over(town)
        else
          town.game_info.status = GameInfo::STATUS_VOTE
          town.game_info.next
          town.next_leader
          push_obj.data += "\n次のミッションに進みます。"
          push.send(town.group_id, push_obj.to_message)
          send_game_info(town.group_id, town)
        end

      end
      town.token_pool.clear
    else
      push_obj.data = "#{town.find_user_name(@from_id)}さんの投票を受け付けました。あと#{town.game_info.current_mission.total_num - town.token_pool.mission_token_list.size}人です。"
      push.send(town.group_id, push_obj.to_message)
    end

    res = MessageObject.new
    res.data = "任務を受け付けました。"

    return res

  end

  private
  def town_by_id()

    puts "town_hash=#{@@town_hash}"
    puts "ルーティングテーブル=#{@@routing_table}"
    town_id = @@routing_table[@from_id]
    puts "村ID自動取得=#{town_id}"
    return @@town_hash[town_id]

  end

  private
  def game_over(town)
    if town.blue_win?
      if town.has_assassin?
        town.game_info.status = GameInfo::STATUS_ASSASSIN
        assassin_id = town.find_assassin_id
        candidates = town.kill_candidates

        str = "誰を殺しましょうか？"
        candidates.each_with_index {|player, i|
          str += "\n#{i+1}: #{player.user_profile.display_name}"
        }
        str += "\nコマンド=【/kill 数字】【/k 数字】"

        push = PushMessage.new
        obj = MessageObject.new
        obj.data = str
        push.send(assassin_id, obj.to_message)

        obj = MessageObject.new
        obj.data = "暗殺者が暗躍しているようです。"
        push.send(town.group_id, obj.to_message)

      else
        blue_win(town)
      end
    else
      red_win(town)
    end
  end

  private
  def kill()

    unless @from_type == "user"

      res = MessageObject.new
      res.data = "暗殺は個別チャットから行ってください。 コマンド名=#{@command}"
      return res

    end

    unless @params.size == 1
      res = MessageObject.new
      res.data = "引数の数が不正です。\n暗殺対象の数値を入力してください。 コマンド名=#{@command}"
      return res
    end

    unless @@routing_table.include?(@from_id)
      res = MessageObject.new
      res.data = "入場している村がありません。 コマンド名=#{@command}"
      return res
    end

    town = town_by_id()

    unless town.find_assassin_id() == @from_id
      res = MessageObject.new
      res.data = "暗殺を実行できるのは暗殺者のみです。 コマンド名=#{@command}"
      return res
    end

    unless town.game_info.status == GameInfo::STATUS_ASSASSIN
      res = MessageObject.new
      res.data = "今は暗殺できません。コマンド名=#{@command}"
      return res
    end

    unless @params[0].to_i.between?(1, town.kill_candidates().size)
      res = MessageObject.new
      res.data = "引数は1..#{town.kill_candidates().size}で入力してください。 コマンド名=#{@command}"
      return res
    end

    index = @params[0].to_i - 1

    target = town.kill_candidates[index]
    push = PushMessage.new
    if target.role.role_id == Role::MARLIN
      obj = MessageObject.new
      obj.data = "見事、マーリンの#{target.user_profile.display_name}さんを殺害しました!!!"
      push.send(town.group_id, obj.to_message)
      red_win(town)
    else
      obj = MessageObject.new
      obj.data = "#{target.user_profile.display_name}さんを殺害しましたがマーリンではありませんでした!!!"
      push.send(town.group_id, obj.to_message)
      blue_win(town)
    end

    res = MessageObject.new
    res.data = "息の根を止めてやりました。"

    return res

  end


  private
  def blue_win(town)
    push = PushMessage.new
    obj = MessageObject.new
    obj.data = "青チームの勝利です!!!\n" + town.show_roles
    town.game_info.status = GameInfo::STATUS_END
    push.send(town.group_id, obj.to_message)
  end

  private
  def red_win(town)
    push = PushMessage.new
    obj = MessageObject.new
    obj.data = "赤チームの勝利です!!!\n" + town.show_roles
    town.game_info.status = GameInfo::STATUS_END
    push.send(town.group_id, obj.to_message)
  end

  private
  def help()
    res = MessageObject.new

    str = <<-"EOS"
＊いつでも使えるコマンド＊
/h,help 使い方を調べる
/i,instruction [0..5] 説明書を開く
/d?,deck? [5..10] プレイヤー人数別、配役デッキ設定を確認

＊ゲーム進行用コマンド＊
【グループチャット、個人チャットを使い分けてBOTに話しかけます。】
/m,make [村ID] 村作成（グループ）
/j,join [村ID] 村参加（個人）
/s,start 作成した村のゲーム開始（グループ）
/v,vote [a,approve/r,reject] チーム認可投票（個人）
/q,quest [s,success/f,fail] 任務成否提出（個人）
/k,kill [1..n] 暗殺実行（個人）

＊ゲーム中に使えるコマンド＊
/d,deck [1..3] デッキ変更（グループ）
/? 進行状況確認（グループ）（個人）
/c,co パーシヴァルCO、CO撤回（個人）
/l,log 過去の投票結果、任務成否ログ（グループ）（個人）
/n,note ログ出力用のメモを取る（グループ）（個人）
EOS
    res.data = str

    return res

  end

  private
  def question()

    unless @@routing_table.include?(@from_id)
      res = MessageObject.new
      res.data = "入場している村がありません。 コマンド名=#{@command}"
      return res
    end

    town = town_by_id()
    res = MessageObject.new

    if town.game_info.status == GameInfo::STATUS_JOIN
      res.data = "新しい村（ID=【#{town.town_id}】）の入室待ちです。\n個別にBOTに話しかけて入室してください。\nコマンド=【/join #{town.town_id}】"
    elsif town.game_info.status == GameInfo::STATUS_VOTE
      res.data = town.game_info.lead_vote_str + "\n" + town.leader_str
    elsif town.game_info.status == GameInfo::STATUS_MISSION
      res.data = "ミッションに選ばれた人は、BOTに個別に\n成功【/quest success】\n失敗【/quest fail】\nを送信してください。\n【/q s】または【/q f】でも構いません。"
    elsif town.game_info.status == GameInfo::STATUS_ASSASSIN
      res.data = "暗殺者による暗殺待ちです。コマンド=【/kill 数字】【/k 数字】"
    end

    return res

  end

  private
  def co()

    unless @from_type == "user"

      res = MessageObject.new
      res.data = "COは個人チャットから行ってください。 コマンド名=#{@command}"
      return res

    end

    unless @@routing_table.include?(@from_id)
      res = MessageObject.new
      res.data = "入場している村がありません。 コマンド名=#{@command}"
      return res
    end

    town = town_by_id()

    unless town.game_info.status == GameInfo::STATUS_VOTE || town.game_info.status == GameInfo::STATUS_MISSION
      res = MessageObject.new
      res.data = "現在はCOを受け付けていません。コマンド名=#{@command}"
      return res
    end

    res = MessageObject.new

    player = town.find_player(@from_id)

    if player.co?

      push = PushMessage.new
      obj = MessageObject.new
      obj.data = "#{player.user_profile.display_name}さんが、自らがパーシヴァルであるとの宣言を撤回しました。"
      push.send(town.group_id, obj.to_message)

      note = Note.new
      note.from_id = town.group_id
      note.from_type = "system"
      note.text = "CO撤回 > #{player.user_profile.display_name}"
      town.log.push(note)

      player.co = false
      res.data = "COを撤回しました。"

    else

      push = PushMessage.new
      obj = MessageObject.new
      obj.data = "#{player.user_profile.display_name}さんが、我こそがパーシヴァルであると名乗り出ました。"
      push.send(town.group_id, obj.to_message)

      note = Note.new
      note.from_id = town.group_id
      note.from_type = "system"
      note.text = "CO > #{player.user_profile.display_name}"
      town.log.push(note)

      player.co = true
      res.data = "COしました。"

    end

    return res

  end

  private
  def log()

    unless @@routing_table.include?(@from_id)
      res = MessageObject.new
      res.data = "入場している村がありません。 コマンド名=#{@command}"
      return res
    end

    town = town_by_id()
    res = MessageObject.new

    if town.log.empty?
      res = MessageObject.new
      res.data = "まだログがありません。コマンド名=#{@command}"
    else
      res = MessageObject.new
      if @from_type == "user"
        res.data = town.log.select{|note| (note.from_id == @from_id) || (note.from_type != "user") }.map{|note| note.headed_text }.join("\n")
      else
        if town.game_info.status == GameInfo::STATUS_END
          res.data = town.log.map{|note|
            str = note.headed_text
            if note.from_type == "user"
              str = "★" + town.find_user_name(note.from_id) + str
            end
            str
          }.join("\n")
        else
          res.data = town.log.select{|note| note.from_type != "user" }.map{|note| note.headed_text }.join("\n")
        end
      end
    end

    max_length = 1900

    if res.data.length > max_length

      array = res.data.scan(/.{1,#{max_length}}/m)

      array.each_with_index {|str, i|

        if i == array.length - 1
          res.data = str
        else
          push = PushMessage.new
          obj = MessageObject.new
          obj.data = str
          push.send(@from_id, obj.to_message)
        end

      }

    end

    return res

  end

  private
  def note()

    unless @@routing_table.include?(@from_id)
      res = MessageObject.new
      res.data = "入場している村がありません。 コマンド名=#{@command}"
      return res
    end

    unless @params.size >= 1
      res = MessageObject.new
      res.data = "引数の数が不正です。1つ以上のメモを入力してください。 コマンド名=#{@command}"
      return res
    end

    town = town_by_id()
    res = MessageObject.new

    note = Note.new
    note.from_id = @from_id
    note.from_type = @from_type
    note.text = @params.join("\n")
    town.log.push(note)

    puts town.log

    res = MessageObject.new
    res.data = "ノートを保存しました。 コマンド名=#{@command}"

    return res

  end

  private
  def deck()

    unless @@routing_table.include?(@from_id)
      res = MessageObject.new
      res.data = "入場している村がありません。 コマンド名=#{@command}"
      return res
    end

    if @from_type == "user"

      res = MessageObject.new
      res.data = "デッキの変更はメインのグループチャット内から行ってください。 コマンド名=#{@command}"
      return res

    end

    unless @params.size == 1
      res = MessageObject.new
      res.data = "引数の数が不正です。設定するデッキ番号を入力してください。 コマンド名=#{@command}"
      return res
    end

    unless @params[0].to_i.between?(1, 4)
      res = MessageObject.new
      res.data = "引数は1..4で入力してください。 コマンド名=#{@command}"
      return res
    end

    town = town_by_id()

    unless town.game_info.status == GameInfo::STATUS_JOIN

      res = MessageObject.new
      res.data = "すでにゲームがはじまっています。 村ID=#{town.town_id}"
      return res

    end

    town.deck_type = @params[0].to_i

    res = MessageObject.new
    res.data = "使用するデッキを#{town.deck_type}番に変更しました。 コマンド名=#{@command}"

    return res

  end

  private
  def deck_question()

    unless @params.size == 1
      res = MessageObject.new
      res.data = "引数の数が不正です。参照したいデッキのプレイヤー数を入力してください。 コマンド名=#{@command}"
      return res
    end

    unless @params[0].to_i.between?(5, 10)
      res = MessageObject.new
      res.data = "引数は5..10で入力してください。 コマンド名=#{@command}"
      return res
    end

    res = MessageObject.new
    res.data = RoleDeck.deck_by_playernum(@params[0].to_i)
    return res

  end

  private
  def debug()

    res = MessageObject.new

    if $debug_mode

      $debug_mode = false

      res.data = "デバッグモードを終了します。"

    else

      $debug_mode = true

      res.data = "デバッグモードに切り替えました。"

    end

    return res

  end

end
