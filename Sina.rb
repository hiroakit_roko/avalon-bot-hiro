require 'sinatra'
require 'line/bot'
require './AvalonResponseFactory'

set :bind, '0.0.0.0'

$IMG_FOLDER_NAME = "images"
$TEMP_FOLDER_NAME = "temp"
$server_address = "unknown"

get '/' do
  "I am hiroakit"
end

get "/#{$IMG_FOLDER_NAME}/:file.:ext" do |file, ext|
  content_type ext
  send_file "#{$IMG_FOLDER_NAME}/#{file}.#{ext}"
end

get "/#{$TEMP_FOLDER_NAME}/:file.:ext" do |file, ext|
  content_type ext
  send_file "#{$TEMP_FOLDER_NAME}/#{file}.#{ext}"
end

def client
  @client ||= Line::Bot::Client.new { |config|
    config.channel_secret = ENV["LINE_CHANNEL_SECRET"]
    config.channel_token = ENV["LINE_CHANNEL_TOKEN"]
  }
end

post '/callback' do

  $server_address = request.scheme + "://" + request.host

  puts "$server_address=#{$server_address}"

  body = request.body.read

  puts "post, request=#{body}"

  signature = request.env['HTTP_X_LINE_SIGNATURE']

  unless client.validate_signature(body, signature)
    error 400 do 'Bad Request' end
  end

  events = client.parse_events_from(body)
  events.each { |event|
    case event
    when Line::Bot::Event::Message
      case event.type
      when Line::Bot::Event::MessageType::Text

        factory = AvalonResponseFactory.new(event)

        puts "入力パラメータ確認!!!! #{factory}"

        if factory.reply?()

          res = factory.reply()
          message = res.to_message()

          puts "post, reply=#{message}"
          client.reply_message(event['replyToken'], message)

        end

    when Line::Bot::Event::MessageType::Image, Line::Bot::Event::MessageType::Video
        response = client.get_message_content(event.message['id'])
        tf = Tempfile.open("content")
        tf.write(response.body)
      end
    end
  }

  "OK"
end
