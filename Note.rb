class Note

  attr_accessor :text
  attr_accessor :from_type
  attr_accessor :from_id

  def to_s
    return "Note: from_id=#{from_id}, from_type=#{from_type}, text=#{text}"
  end

  def headed_text
    str = ""
    if from_type != "system"
      str = ">" + text.gsub(/(\n)/, "\n>")
    else
      str = text
    end
    return str
  end

end
