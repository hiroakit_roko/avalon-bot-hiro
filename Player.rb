class Player

  attr_accessor :role
  attr_accessor :leader
  alias :leader? :leader
  attr_accessor :user_profile
  attr_accessor :co
  alias :co? :co

  def initialize()
    @co = false
  end

  def to_s
    str = ""
    unless user_profile.nil?
      str += user_profile.to_s
    else
      str += "user_profile=nil"
    end
    str += ","
    unless role.nil?
      str += role.to_s
    else
      str += "role=nil"
    end
    return str
  end

end
